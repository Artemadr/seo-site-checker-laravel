<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SiteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::pluck('id')->toArray();

        return [
            'url' => $this->faker->url(),
            'check_domain' => intval(rand(0, 1)),
            'check_ssl' => intval(rand(0, 1)),
            'user_id' => $this->faker->randomElement($users),
        ];
    }
}
