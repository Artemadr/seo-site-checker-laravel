@foreach($errors as $url => $siteErrors)
    <strong>{{ $url }}:</strong>
    <div>
        @foreach($siteErrors as $error)
            {{ $error }}<br>
        @endforeach
    </div>
    <hr>
@endforeach
