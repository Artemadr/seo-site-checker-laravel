@extends('layouts.app')

@section('content')
                <div class="card">
                    <div class="card-header">Добавление сайта</div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <form method="post" class="w-50" action="{{ route('site.store') }}">
                                    @csrf
                                    <div class="form-group mb-2">
                                        <label for="url">Url сайта</label>
                                        <input type="text" name="url" class="form-control" id="url" aria-describedby="emailHelp" placeholder="http://site.ru/" maxlength="255" value="{{ old('url') }}">
                                        @error('url')
                                            <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-check">
                                        <input type="hidden" name="check_ssl" value="0">
                                        <input class="form-check-input" type="checkbox" name="check_ssl" value="1" id="check_ssl" {{ old('check_ssl') == '1' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="check_ssl">
                                            Включить проверку сертификатов SSL
                                        </label>
                                    </div>

                                    <div class="form-check mb-4">
                                        <input type="hidden" name="check_domain" value="0">
                                        <input class="form-check-input" type="checkbox" name="check_domain" value="1" id="check_domain" {{ old('check_domain') == '1' ? 'checked' : '' }}>
                                        <label class="form-check-label" for="check_domain">
                                            Включить проверку домена
                                        </label>
                                    </div>

                                    <button type="submit" class="btn btn-success">Создать</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
