@extends('layouts.app')

@section('content')
                <div class="card">
                    <div class="card-header">Просмотр сайта <a href="{{ route('site.edit', $site->id) }}"><i
                                class="fa-solid fa-pen"></i></a></div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <p><a class="btn-link" href="{{ route('site.index') }}">Список сайтов</a></p>
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Url сайта</th>
                                        <td>{{ $site->url }}</td>
                                    </tr>
                                    <tr>
                                        <td>SSL</td>
                                        <td>{{ $site->check_ssl?'Да':'Нет' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Домен</td>
                                        <td>{{ $site->check_domain?'Да':'Нет' }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
