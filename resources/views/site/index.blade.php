@extends('layouts.app')

@section('content')
                <div class="card">
                    <div class="card-header">Список сайтов</div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <p><a class="btn-link" href="{{ route('site.create') }}">Добавить сайт</a></p>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Url сайта</th>
                                        <th scope="col" colspan="3"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sites as $site)
                                        <tr>
                                            <th scope="row"><a
                                                    href="{{ route('site.edit', $site->id) }}">{{ $site->url }}</a></th>
                                            <td><a href="{{ route('site.show', $site->id) }}"><i
                                                        class="fa-solid fa-eye"></i></a>
                                            </td>
                                            <td>
                                                <a href="{{ route('site.edit', $site->id) }}"><i
                                                        class="fa-solid fa-pen"></i></a>
                                            </td>
                                            <td>
                                                <form action="{{ route('site.delete', $site->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="border-0 bg-transparent">
                                                        <i class="fa-solid fa-trash text-danger"></i>
                                                    </button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
