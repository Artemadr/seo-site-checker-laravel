@extends('layouts.app')

@section('content')
                <div class="card">
                    <div class="card-header">Изменение сайта <a href="{{ route('site.show', $site->id) }}"><i
                                class="fa-solid fa-eye"></i></a></div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <p><a class="btn-link" href="{{ route('site.index') }}">Список сайтов</a></p>
                                <form method="post" class="w-50" action="{{ route('site.update', $site->id) }}">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group mb-2">
                                        <label for="url">Url сайта</label>
                                        <input type="text" name="url" class="form-control" id="url"
                                               aria-describedby="emailHelp" placeholder="http://site.ru/"
                                               maxlength="255" value="{{ $site->url }}">
                                        @error('url')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-check">
                                        <input type="hidden" name="check_ssl" value="0">
                                        <input class="form-check-input" type="checkbox" name="check_ssl" value="1"
                                               id="check_ssl" @if($site->check_ssl) checked @endif>
                                        <label class="form-check-label" for="check_ssl">
                                            Включить проверку сертификатов SSL
                                        </label>
                                    </div>

                                    <div class="form-check mb-4">
                                        <input type="hidden" name="check_domain" value="0">
                                        <input class="form-check-input" type="checkbox" name="check_domain" value="1"
                                               id="check_domain" @if($site->check_domain) checked @endif>
                                        <label class="form-check-label" for="check_domain">
                                            Включить проверку домена
                                        </label>
                                    </div>

                                    <button type="submit" class="btn btn-success">Обновить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
