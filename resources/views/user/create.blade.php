@extends('layouts.app')

@section('content')
                <div class="card">
                    <div class="card-header">Добавление пользователя</div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <form method="post" class="w-50" action="{{ route('user.store') }}">
                                    @csrf
                                    <div class="form-group mb-2">
                                        <label for="name">Имя</label>
                                        <input type="text" name="name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Иван" maxlength="255" value="{{ old('name') }}">
                                        @error('name')
                                            <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-2">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" class="form-control" id="email"
                                               aria-describedby="emailHelp" placeholder="ivan@mail.ru"
                                               maxlength="255" value="{{ old('email') }}">
                                        @error('email')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-2">
                                        <label for="password">Пароль</label>
                                        <input type="text" name="password" class="form-control" id="password"
                                               aria-describedby="emailHelp" placeholder="*******"
                                               maxlength="255" value="">
                                        @error('password')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-2">
                                        <label for="role">Роль</label>
                                        <select name="role" class="form-control" id="role">
                                            @foreach($roles as $id=>$role)
                                            <option value="{{ $id }}" @if(old('role') !== null && old('role') == $id ) selected @endif>{{ $role }}</option>
                                            @endforeach
                                        </select>
                                        @error('role')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-success">Создать</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
