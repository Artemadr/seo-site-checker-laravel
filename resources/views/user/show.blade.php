@extends('layouts.app')

@section('content')
                <div class="card">
                    <div class="card-header">Просмотр пользователя <a href="{{ route('user.edit', $user->id) }}"><i
                                class="fa-solid fa-pen"></i></a></div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <p><a class="btn-link" href="{{ route('user.index') }}">Список пользователей</a></p>
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th scope="row">Имя</th>
                                        <td>{{ $user->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
