@extends('layouts.app')

@section('content')
                <div class="card">
                    <div class="card-header">Список пользователей</div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <p><a class="btn-link" href="{{ route('user.create') }}">Добавить пользователя</a></p>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">Имя</th>
                                        <th scope="col" colspan="3"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <th scope="row"><a
                                                    href="{{ route('user.edit', $user->id) }}">{{ $user->name }}</a></th>
                                            <td><a href="{{ route('user.show', $user->id) }}"><i
                                                        class="fa-solid fa-eye"></i></a>
                                            </td>
                                            <td>
                                                <a href="{{ route('user.edit', $user->id) }}"><i
                                                        class="fa-solid fa-pen"></i></a>
                                            </td>
                                            <td>
                                                <form action="{{ route('user.delete', $user->id) }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="border-0 bg-transparent">
                                                        <i class="fa-solid fa-trash text-danger"></i>
                                                    </button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
