@extends('layouts.app')

@section('content')
                <div class="card">
                    <div class="card-header">Изменение пользователя <a href="{{ route('user.show', $user->id) }}"><i
                                class="fa-solid fa-eye"></i></a></div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <p><a class="btn-link" href="{{ route('user.index') }}">Список пользователей</a></p>
                                <form method="post" class="w-50" action="{{ route('user.update', $user->id) }}">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group mb-2">
                                        <label for="name">Имя</label>
                                        <input type="text" name="name" class="form-control" id="name" aria-describedby="emailHelp" placeholder="Иван" maxlength="255" value="{{ $user->name }}">
                                        @error('name')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-2">
                                        <label for="email">Email</label>
                                        <input type="text" name="email" class="form-control" id="email"
                                               aria-describedby="emailHelp" placeholder="ivan@mail.ru"
                                               maxlength="255" value="{{ $user->email }}">
                                        @error('email')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-2">
                                        <label for="role">Роль</label>
                                        <select name="role" class="form-control" id="role">
                                            @foreach($roles as $id=>$role)
                                                <option value="{{ $id }}" @if($user->role == $id ) selected @endif>{{ $role }}</option>
                                            @endforeach
                                        </select>
                                        @error('role')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>

                                    <input type="hidden" name="user_id" value="{{ $user->id }}">

                                    <button type="submit" class="btn btn-success">Обновить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
@endsection
