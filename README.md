## About Site checker

Application allows to register users, set them roles. Users can create links to sites. Application checkes sites for:
- Domain expiration
- SSL expiration

If any errors were found, application notifies users.

## Technologies
- Laravel 8
- Docker compose
- Email
- io-developer/php-whois

## Usage
- add to env: CHECKER_DAYS_LIMIT_TO_EXPIRATION
- start checking procedure with command <code>php artisan sitescheck:start</code>

## License
The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
