<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class Site extends Model
{
    use HasFactory;
    use softDeletes;

    protected $table = "sites";
    protected $guarded = false;

    function scopeForCurrentUser(Builder $query): Builder
    {
        return $query->where('user_id', '=', Auth::user()->id);
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }

}
