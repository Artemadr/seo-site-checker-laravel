<?php

namespace App\Console\Commands;

use App\Services\MainValidator;
use Illuminate\Console\Command;

class StartSitesCheckCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitescheck:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start sites checking';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $command = new MainValidator();
        $command();
        return true;
    }
}
