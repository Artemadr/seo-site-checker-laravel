<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Site;

class EditController extends Controller
{
    public function __invoke(Site $site)
    {
        return view( 'site.edit', compact('site'));
    }
}
