<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Site;

class ShowController extends Controller
{
    public function __invoke(Site $site)
    {
        return view( 'site.show', compact('site'));
    }
}
