<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Site;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function __invoke()
    {
        if (Auth::user()->isAdmin())
            $sites = Site::all();
        else
            $sites = Site::query()->forCurrentUser()->get();

        return view( 'site.index', compact('sites'));
    }
}
