<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\StoreRequest;
use App\Models\Site;
use Illuminate\Support\Facades\Auth;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();

        $data['user_id'] = Auth::user()->id;

        Site::firstorCreate($data);

        return redirect()->route('site.index');
    }
}
