<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\UpdateRequest;
use App\Models\Site;

class DeleteController extends Controller
{
    public function __invoke(Site $site)
    {
        $site->delete();

        return redirect()->route('site.index');
    }
}
