<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\Site\UpdateRequest;
use App\Models\Site;
use Illuminate\Support\Facades\Auth;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request, Site $site)
    {
        if ($site->user_id == Auth::user()->id || Auth::user()->isAdmin())
            $site->update(
                $request->validated()
            );

        return redirect()->route('site.show', compact('site'));
    }
}
