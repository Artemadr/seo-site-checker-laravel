<?php

namespace App\Services\Interfaces;

use App\Models\User;

interface ErrorSenderInterface
{
    public function prepare(User $user);

    public function canSend(): bool;

    public function send(array $errors);
}
