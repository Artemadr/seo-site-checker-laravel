<?php

namespace App\Services;

use App\Models\Site;
use App\Models\User;
use App\Services\Interfaces\ErrorSenderInterface;
use Illuminate\Database\Eloquent\Collection;

class UserNotifier
{
    /**
     * @var User|mixed
     */
    protected User $user;
    protected array $errors = [];
    protected array $errorSenders = [];

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param ErrorSenderInterface $errorSender
     * @return self
     */
    public function addErrorSender(ErrorSenderInterface $errorSender): self
    {
        $this->errorSenders[] = $errorSender;
        return $this;
    }

    public function notifyIfErrorInSites(Collection $sites)
    {
        foreach ($sites as $site) {
            $siteErrors = (new SiteValidator($site))->validateAllAndGetErrors();
            $this->addSiteToListIfHasErrors($site, $siteErrors);
        }
        $this->informUserIfAnyErrors();
    }

    /**
     * @param Site $site
     * @param array $errors
     */
    private function addSiteToListIfHasErrors(Site $site, array $errors)
    {
        if (count($errors)) {
            $this->errors[$site->url] = $errors;
        }
    }

    /**
     * @return void
     */
    private function informUserIfAnyErrors()
    {
        if (count($this->errors)) {
            $this->informUser();
        }
    }

    /**
     * @return void
     */
    protected function informUser(): void
    {
        foreach ($this->errorSenders as $errorSender) {
            $errorSender->prepare($this->user);
            if ($errorSender->canSend())
                $errorSender->send($this->errors);
        }
    }

}
