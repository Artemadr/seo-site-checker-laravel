<?php

namespace App\Services;

use App\Models\User;
use App\Services\ErrorSender\MailErrorSender;

class MainValidator
{
    /**
     *
     */
    public function __invoke(): void
    {
        $users = $this->getUsers();
        foreach ($users as $user) {
            $sites = $user->sites;
            if (count($sites)) {
                $userNotifier = new UserNotifier($user);
                $userNotifier->addErrorSender(new MailErrorSender());
                $userNotifier->notifyIfErrorInSites($sites);
            }
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    protected function getUsers()
    {
        return User::all();
    }

}
