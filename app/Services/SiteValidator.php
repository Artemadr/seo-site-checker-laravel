<?php

namespace App\Services;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Iodev\Whois\Factory;

class SiteValidator
{
    public const ERROR_FIND_DOMAIN_DATE = "Не удалось определить дату окончания действия домена";
    public const ERROR_DOMAIN_DATE_EXPIRE = "Дата окончания действия домена подходит к концу";
    public const ERROR_FIND_SSL_DATE = "Не удалось определить дату окончания действия SSL-сертификата";
    public const ERROR_SSL_DATE_EXPIRE = "Дата окончания действия SSL-сертификата подходит к концу";
    public const ERROR_CONNECTION = "Не удается подключиться к сайту";
    public const ERROR_BAD_DOMAIN = "Не удалось определить домен для урла";
    protected Model $site;
    protected array $logs = [];

    /**
     * @param Model $site
     */
    public function __construct(Model $site)
    {
        $this->site = $site;
    }

    /**
     * @return array
     */
    public function validateAllAndGetErrors(): array
    {
        $domain = $this->parseDomainFromUrl($this->site->url);

        if ($domain == '') {
            $this->addToLogs(self::ERROR_BAD_DOMAIN);
            return $this->logs;
        }

        $domain1Level = $this->parseDomain1Level($domain);

        if ($this->site->check_domain) {
            $this->checkDomainExpirationAndLog($domain1Level);
        }

        if ($this->site->check_ssl) {
            if ($this->checkIfDomainExists($domain)) {
                $this->checkSSLExpirationAndLog($domain);
            } else {
                $this->addToLogs(self::ERROR_CONNECTION);
            }
        }

        return $this->logs;
    }

    /**
     *
     */
    protected function parseDomainFromUrl($url): string
    {
        $url = trim($url);
        $domain = parse_url($url, PHP_URL_HOST);
        if ($domain == null) {
            $domain = $url;
        }
        return $this->punicodeDomain($domain);
    }

    /**
     * @param string $domain
     * @return string
     */
    protected function punicodeDomain(string $domain): string
    {
        $pDomain = idn_to_ascii($domain);
        return $pDomain !== false ? $pDomain : "";
    }

    /**
     * @param string $error
     */
    protected function addToLogs(string $error): void
    {
        $this->logs[] = $error;
    }

    /**
     * @param string $domain
     * @return string
     */
    protected function parseDomain1Level(string $domain): string
    {
        if (substr_count($domain, '.') < 2) {
            $domain1Level = $domain;
        } else {
            $domain0Level = substr($domain, strrpos($domain, '.'));
            $domain1Level = substr($domain, 1 + strrpos($domain, '.', -1 - strlen($domain0Level)));
        }
        return $domain1Level;
    }

    /**
     * @param string $domain1Level
     */
    protected function checkDomainExpirationAndLog(string $domain1Level): void
    {
        $dateTimestamp = $this->getDomainExpirationDate($domain1Level);
        if ($dateTimestamp === false) {
            $this->addToLogs(self::ERROR_FIND_DOMAIN_DATE);
            return;
        }

        $daysLimitToExpiration = config('days_limit_to_expiration');

        if (time() + $daysLimitToExpiration * 60 * 60 * 24 > $dateTimestamp) {
            $this->addToLogs(self::ERROR_DOMAIN_DATE_EXPIRE);
        }
    }

    /**
     * @return int|false
     */
    protected function getDomainExpirationDate($domain1Level)
    {
        $whois = Factory::get()->createWhois();

        try {
            $info = $whois->loadDomainInfo($domain1Level);
        } catch (Exception $e) {
            return false;
        }

        return $info->expirationDate ?? false;
    }

    /**
     * @param string $domain
     * @return bool
     */
    protected function checkIfDomainExists(string $domain): bool
    {
        return gethostbyname($domain) != $domain;
    }

    /**
     * @param string $domain
     */
    protected function checkSSLExpirationAndLog(string $domain): void
    {
        $get = stream_context_create(["ssl" => ["capture_peer_cert" => true]]);
        $read = stream_socket_client(
            "ssl://" . $domain . ":443",
            $errno,
            $errstr,
            10,
            STREAM_CLIENT_CONNECT,
            $get
        );
//        if (!$read) {
//            $this->addToLogs("Не удалось подключиться к сайту");
//            return;
//        }
        $cert = stream_context_get_params($read);
        $certinfo = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);

        $daysLimitToExpiration = config('days_limit_to_expiration');

        if (!isset($certinfo['validTo_time_t'])) {
            $this->addToLogs(self::ERROR_FIND_SSL_DATE);
        } elseif (time() + $daysLimitToExpiration * 60 * 60 * 24 > $certinfo['validTo_time_t']) {
            $this->addToLogs(self::ERROR_SSL_DATE_EXPIRE);
        }
    }

}
