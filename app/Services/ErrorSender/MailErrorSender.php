<?php

namespace App\Services\ErrorSender;

use App\Mail\SiteCheckErrorEmail;
use App\Models\User;
use App\Services\Interfaces\ErrorSenderInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

class MailErrorSender implements ErrorSenderInterface
{
    protected Model $user;

    public function prepare(User $user)
    {
        $this->user = $user;
    }

    public function canSend(): bool
    {
        return true;
    }

    public function send(array $errors)
    {
        Mail::to($this->user->email)->send(new SiteCheckErrorEmail($errors));
    }
}
