<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController')->name('home');

Route::group(['namespace'=>'Site', 'prefix'=>'sites', 'middleware'=>'auth'], function () {
    Route::get('/', 'IndexController')->name('site.index');
    Route::get('/create', 'CreateController')->name('site.create');
    Route::post('/', 'StoreController')->name('site.store');
    Route::get('/{site}', 'ShowController')->name('site.show');
    Route::get('/{site}/edit', 'EditController')->name('site.edit');
    Route::patch('/{site}', 'UpdateController')->name('site.update');
    Route::delete('/{site}', 'DeleteController')->name('site.delete');
});

Route::group(['namespace'=>'User', 'prefix'=>'users', 'middleware'=>'auth.admin'], function () {
    Route::get('/', 'IndexController')->name('user.index');
    Route::get('/create', 'CreateController')->name('user.create');
    Route::post('/', 'StoreController')->name('user.store');
    Route::get('/{user}', 'ShowController')->name('user.show');
    Route::get('/{user}/edit', 'EditController')->name('user.edit');
    Route::patch('/{user}', 'UpdateController')->name('user.update');
    Route::delete('/{user}', 'DeleteController')->name('user.delete');
});

Auth::routes();
