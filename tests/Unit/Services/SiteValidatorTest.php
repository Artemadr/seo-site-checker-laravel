<?php

namespace Tests\Unit\Services;

use App\Models\Site;
use App\Models\User;
use App\Services\SiteValidator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Config;
use ReflectionException;
use Tests\TestCase;

class SiteValidatorTest extends TestCase
{
    use DatabaseMigrations;

    private SiteValidator $obj;
    private Model $site;

    /**
     * @codeCoverageIgnore
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create([
            'name' => 'admintest111',
            'email' => 'admintest111@example.com',
            'role' => User::ROLE_ADMIN
        ]);

        $this->site = Site::factory()->create([
            'url' => 'https://yandex.ru/',
            'check_domain' => true,
            'check_ssl' => true,
            'user_id' => $user->id,
        ]);

        $this->obj = new SiteValidator($this->site);
    }

    /**
     * @return void
     * @throws ReflectionException
     */
    public function testCheckDomainExpirationAndLog()
    {
        $mock = $this->getMockBuilder(SiteValidator::class)
            ->setConstructorArgs([$this->site])
            ->onlyMethods(['getDomainExpirationDate'])
            ->getMock();

        $mock->method('getDomainExpirationDate')
            ->will($this->returnValue(false));

        $methodCheckDomainExpirationAndLog = $this->getPrivateMethod(
            SiteValidator::class,
            'checkDomainExpirationAndLog'
        );

        $propertyLogs = $this->getPrivateProperty(SiteValidator::class, 'logs');

        $oldDays = config('days_limit_to_expiration');
        Config::set('days_limit_to_expiration', 0);

        $methodCheckDomainExpirationAndLog->invoke($mock, 'yandex.ru');
        $this->assertEquals([SiteValidator::ERROR_FIND_DOMAIN_DATE], $propertyLogs->getValue($mock));

        $mock = $this->createMock(SiteValidator::class);

        Config::set('days_limit_to_expiration', 9999);
        $propertyLogs->setValue($mock, []);
        $methodCheckDomainExpirationAndLog->invoke($mock, 'yandex.ru');
        $this->assertEquals([SiteValidator::ERROR_DOMAIN_DATE_EXPIRE], $propertyLogs->getValue($mock));

        Config::set('days_limit_to_expiration', -9999);
        $propertyLogs->setValue($mock, []);
        $methodCheckDomainExpirationAndLog->invoke($mock, 'yandex.ru');
        $this->assertEquals([], $propertyLogs->getValue($mock));

        Config::set('days_limit_to_expiration', $oldDays);
    }

    /**
     * @return void
     */
    public function testValidateAllAndGetLogs_BadDomain()
    {
        $mock = $this->getMockBuilder(SiteValidator::class)
            ->setConstructorArgs([$this->site])
            ->onlyMethods(['parseDomainFromUrl'])
            ->getMock();

        $mock->method('parseDomainFromUrl')
            ->will($this->returnValue(''));

        $this->assertCount(1, $mock->validateAllAndGetErrors());
    }

    /**
     * @return void
     */
    public function testvalidateAllAndGetErrors_DomainNotExists()
    {
        $mock = $this->getMockBuilder(SiteValidator::class)
            ->setConstructorArgs([$this->site])
            ->onlyMethods(
                [
                    'parseDomainFromUrl',
                    'parseDomain1Level',
                    'checkDomainExpirationAndLog',
                    'checkIfDomainExists'
                ]
            )
            ->getMock();

        $mock->method('parseDomainFromUrl')
            ->will($this->returnValue('example.com'));
        $mock->method('parseDomain1Level')
            ->will($this->returnValue('example.com'));
        $mock->method('checkDomainExpirationAndLog')
            ->will($this->returnValue(null));
        $mock->method('checkIfDomainExists')
            ->will($this->returnValue(false));

        $this->assertCount(1, $mock->validateAllAndGetErrors());
    }

    /**
     * @dataProvider providerParseDomainFromUrl
     * @return void
     * @throws ReflectionException
     */
    public function testParseDomainFromUrl($url, $expected)
    {
        $methodParseDomainFromUrl = $this->getPrivateMethod(SiteValidator::class, 'parseDomainFromUrl');

        $this->assertEquals($expected, $methodParseDomainFromUrl->invoke($this->obj, $url));
    }

    /**
     * @codeCoverageIgnore
     * @return string[][]
     */
    public function providerParseDomainFromUrl(): array
    {
        return [
            'usual https' => ['https://avtopremier.net/', 'avtopremier.net'],
            'long with http' => ['http://yandex.ru/test/123.php#asd ', 'yandex.ru'],
            'subdomain' => ['https://rostov-na-dony.demidovsteel.ru/', 'rostov-na-dony.demidovsteel.ru'],
            'nbsp' => [' https://atery ', 'atery'],
            'long domain 1 lvl' => ['test.proverka', 'test.proverka'],
            'punicode1' => ['ftp://xn--e1ajeds9e.xn--p1ai/', 'xn--e1ajeds9e.xn--p1ai'],
            'punicode2' => ['https://кремль.рф/', 'xn--e1ajeds9e.xn--p1ai'],
        ];
    }

    /**
     * @dataProvider providerParseDomain1Level
     * @return void
     * @throws ReflectionException
     */
    public function testParseDomain1Level($domain, $expected)
    {
        $methodParseDomainDomain1Level = $this->getPrivateMethod(SiteValidator::class, 'parseDomain1Level');

        $this->assertEquals($expected, $methodParseDomainDomain1Level->invoke($this->obj, $domain));
    }

    /**
     * @codeCoverageIgnore
     * @return string[][]
     */
    public function providerParseDomain1Level(): array
    {
        return [
            'usual https' => ['avtopremier.net', 'avtopremier.net'],
            'long with http' => ['yandex.ru', 'yandex.ru'],
            'subdomain' => ['rostov-na-dony.demidovsteel.ru', 'demidovsteel.ru'],
            'long domain 1 lvl' => ['test.proverka', 'test.proverka'],
            'punicode1' => ['xn--e1ajeds9e.xn--p1ai', 'xn--e1ajeds9e.xn--p1ai'],
            //'punicode-impossible' => ['кремль.рф', 'кремль.рф'],
        ];
    }

    /**
     * @dataProvider providerCheckSslExpirationAndLog
     * @return void
     * @throws ReflectionException
     */
    public function testCheckSslExpirationAndLog($domain, $days, $expected)
    {
        $methodCheckSSLExpirationAndLog = $this->getPrivateMethod(
            SiteValidator::class,
            'checkSSLExpirationAndLog'
        );
        $propertyLogs = $this->getPrivateProperty(SiteValidator::class, 'logs');

        $oldDays = config('days_limit_to_expiration');
        Config::set('days_limit_to_expiration', $days);

        $methodCheckSSLExpirationAndLog->invoke($this->obj, $domain);
        $this->assertCount($expected, $propertyLogs->getValue($this->obj));

        Config::set('days_limit_to_expiration', $oldDays);
    }

    /**
     * @codeCoverageIgnore
     * @return string[][]
     */
    public function providerCheckSslExpirationAndLog(): array
    {
        return [
            'usual' => ['yandex.ru', 1, 0],
            'punicode' => ['xn--d1aqf.xn--p1ai', 1, 0],
            'longrange' => ['contactgroup.ru', 3 * 365, 1],
            //'not-existing' => ['ololol.olololo', 1, 0],
            //'punicode-impossible' => ['кремль.рф', 3 * 365, 1],
        ];
    }

    /**
     * @dataProvider providerCheckIfDomainExists
     * @return void
     * @throws ReflectionException
     */
    public function testCheckIfDomainExists($domain, $expected)
    {
        $methodCheckIfDomainExists = $this->getPrivateMethod(SiteValidator::class, 'checkIfDomainExists');

        $this->assertEquals($expected, $methodCheckIfDomainExists->invoke($this->obj, $domain));
    }

    /**
     * @codeCoverageIgnore
     * @return string[][]
     */
    public function providerCheckIfDomainExists(): array
    {
        return [
            'usual' => ['yandex.ru', true],
            'not-existing' => ['ololol.olololo', false],
            'punicode' => ['xn--d1aqf.xn--p1ai', true],
            //'punicode-impossible' => ['дом.рф', true],
        ];
    }

    /**
     * @dataProvider providerGetDomainExpirationDate
     * @return void
     * @throws ReflectionException
     */
    public function testGetDomainExpirationDate($domain1Level, $expected)
    {
        $methodCheckIfDomainExists = $this->getPrivateMethod(SiteValidator::class, 'getDomainExpirationDate');

        $this->$expected($methodCheckIfDomainExists->invoke($this->obj, $domain1Level));
    }

    /**
     * @codeCoverageIgnore
     * @return string[][]
     */
    public function providerGetDomainExpirationDate(): array
    {
        return [
            'usual' => ['yandex.ru', 'assertIsInt'],
            'not-existing' => ['ololol.olololo', 'assertFalse'],
            'punicode' => ['xn--d1aqf.xn--p1ai', 'assertIsInt'],
        ];
    }

}
