<?php

namespace Controllers\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateControllerTest extends TestCase
{
    use RefreshDatabase;

    protected string $prefixUrl = '/users';
    protected Model $admin;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = User::factory()->create([
            'name' => 'admintest111',
            'email' => 'admintest111@example.com',
            'role' => User::ROLE_ADMIN
        ]);
    }

    public function testStatusAndDbEntry()
    {
        $response = $this->actingAs($this->admin)->patch($this->prefixUrl.'/'.$this->admin->id, [
            'name' => 'usertest111',
            'email' => 'usertest111@example.com',
            'user_id' => $this->admin->id,
            'role' => User::ROLE_USER
        ]);

        $this->assertDatabaseHas('users', [
            'name' => 'usertest111',
            'email' => 'usertest111@example.com',
            'role' => User::ROLE_USER
        ]);

        $response->assertRedirect($this->prefixUrl.'/'.$this->admin->id);
    }

}
