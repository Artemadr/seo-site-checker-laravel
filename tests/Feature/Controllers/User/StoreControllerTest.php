<?php

namespace Controllers\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreControllerTest extends TestCase
{
    use RefreshDatabase;

    protected string $prefixUrl = '/users';
    protected Model $admin;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = User::factory()->create([
            'name' => 'admintest111',
            'email' => 'admintest111@example.com',
            'role' => User::ROLE_ADMIN
        ]);
    }

    public function testStatusAndDbEntry()
    {
        $response = $this->actingAs($this->admin)->post($this->prefixUrl, [
            'name' => 'usertest111',
            'email' => 'usertest111@example.com',
            'password' => 'paSSword123!',
            'role' => User::ROLE_USER
        ]);

        $response->assertRedirect($this->prefixUrl);

        $this->assertDatabaseHas('users', [
            'email' => 'usertest111@example.com',
        ]);
    }

    public function testDbEntryWithoutRequiredField()
    {
        $this->actingAs($this->admin)->post($this->prefixUrl, [
            'name' => 'usertest111',
            'email' => 'usertest111@example.com',
            'password' => 'paSSword123!',
            //'role' => User::ROLE_USER
        ]);

        $this->assertDatabaseMissing('users', [
            'email' => 'usertest111@example.com',
        ]);
    }
}
