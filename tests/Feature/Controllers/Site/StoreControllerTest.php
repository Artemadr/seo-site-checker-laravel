<?php

namespace Controllers\Site;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreControllerTest extends TestCase
{
    use RefreshDatabase;

    protected string $prefixUrl = '/sites';
    protected Model $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create([
            'name' => 'admintest111',
            'email' => 'admintest111@example.com',
            'role' => User::ROLE_ADMIN
        ]);
    }

    public function testStatusAndDbEntry()
    {
        $response = $this->actingAs($this->user)->post($this->prefixUrl, [
            'url' => 'https://example.com/',
            'check_domain' => true,
            'check_ssl' => true,
        ]);

        $response->assertRedirect($this->prefixUrl);

        $this->assertDatabaseHas('sites', [
            'url' => 'https://example.com/',
        ]);
    }

    public function testDbEntryWithoutRequiredField()
    {
        $this->actingAs($this->user)->post($this->prefixUrl, [
            'url' => 'https://example.com/',
            //'check_domain' => true,
            //'check_ssl' => true,
        ]);

        $this->assertDatabaseMissing('sites', [
            'url' => 'https://example.com/',
        ]);
    }
}
