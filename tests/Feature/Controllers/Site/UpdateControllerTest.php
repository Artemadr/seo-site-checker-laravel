<?php

namespace Controllers\Site;

use App\Models\Site;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateControllerTest extends TestCase
{
    use RefreshDatabase;

    protected string $prefixUrl = '/sites';
    protected Model $user;
    protected Model $admin;
    protected Model $usersite;
    protected Model $adminsite;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = User::factory()->create([
            'name' => 'admintest111',
            'email' => 'admintest111@example.com',
            'role' => User::ROLE_ADMIN
        ]);

        $this->user = User::factory()->create([
            'name' => 'usertest111',
            'email' => 'usertest111@example.com',
            'role' => User::ROLE_USER
        ]);

        $this->usersite = Site::factory()->create([
            'url' => 'https://usersite.ru/',
            'check_domain' => true,
            'check_ssl' => true,
            'user_id' => $this->user->id,
        ]);

        $this->adminSite = Site::factory()->create([
            'url' => 'https://adminsite.com/',
            'check_domain' => true,
            'check_ssl' => true,
            'user_id' => $this->admin->id,
        ]);
    }

    public function testStatusAndDbEntry()
    {
        $response = $this->actingAs($this->user)->patch($this->prefixUrl . '/' . $this->usersite->id, [
            'url' => 'https://example.com/',
            'check_domain' => true,
            'check_ssl' => true,
        ]);

        $this->assertDatabaseHas('sites', [
            'url' => 'https://example.com/',
        ]);

        $response->assertRedirect($this->prefixUrl . '/' . $this->usersite->id);
    }

    public function testUserActsOtherUserSite()
    {
        $this->actingAs($this->user)->patch($this->prefixUrl . '/' . $this->adminSite->id, [
            'url' => 'https://example.com/',
            'check_domain' => true,
            'check_ssl' => true,
        ]);

        $this->assertDatabaseMissing('sites', [
            'url' => 'https://example.com/',
        ]);
    }

    public function testAdminActsOtherUserSite()
    {
        $this->actingAs($this->admin)->patch($this->prefixUrl . '/' . $this->usersite->id, [
            'url' => 'https://example.com/',
            'check_domain' => true,
            'check_ssl' => true,
        ]);

        $this->assertDatabaseHas('sites', [
            'url' => 'https://example.com/',
        ]);
    }

}
