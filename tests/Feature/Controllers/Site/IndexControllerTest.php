<?php

namespace Controllers\Site;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IndexControllerTest extends TestCase
{
    use RefreshDatabase;

    protected string $prefixUrl = '/sites';

    public function testStatusForNoAuth()
    {
        $response = $this->get($this->prefixUrl);
        $response->assertStatus(302);
    }

    public function testStatusForUser()
    {
        $user = User::factory()->create([
            'name' => 'usertest111',
            'email' => 'usertest111@example.com',
            'role' => User::ROLE_USER
        ]);

        $response = $this->actingAs($user)->get($this->prefixUrl);
        $response->assertStatus(200);
    }

    public function testStatusForAdmin()
    {
        $admin = User::factory()->create([
            'name' => 'admintest111',
            'email' => 'admintest111@example.com',
            'role' => User::ROLE_ADMIN
        ]);

        $response = $this->actingAs($admin)->get($this->prefixUrl);
        $response->assertStatus(200);
    }
}
